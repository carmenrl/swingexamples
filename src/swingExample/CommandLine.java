package swingExample;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

public class CommandLine {

   public static void main(String[] args) {
 
      //----------------------------------------------------------------
      // Parse the command-line arguments and print them, or the
      // appropriate error messages, in the numbersText box.
      //
      JTextArea numbersText=new JTextArea(3,30);
      try {
           int n=Integer.parseInt(args[0]);
           numbersText.setText("The "+n+" numbers are\n");
           for(int i=1;i<=n;i++) {
                try {
                    numbersText.append(Integer.parseInt(args[i])+" ");
                }
               catch(NumberFormatException e) {
                    numbersText.append("NaN ");
               }
               // If we catch here, we may get this line multiple times
               //catch(ArrayIndexOutOfBoundsException e) {
               //     numbersText.append("\n Not enough numbers");
               //}
          }
      }
      catch(NumberFormatException e) {
          numbersText.setText("The first argument is not an integer");
      }
      catch(ArrayIndexOutOfBoundsException e) {
          if(args.length==0)
             numbersText.setText("There are no arguments!");
          else
             numbersText.append("\n Not enough numbers");
      }
           
      JFrame window=new JFrame("Nothing");
      window.getContentPane().setLayout(new BorderLayout());

      window.getContentPane().add(numbersText,BorderLayout.SOUTH);
     
      window.pack();
      window.show();

   }
}
