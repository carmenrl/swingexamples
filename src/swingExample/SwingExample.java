package swingExample;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;

import javax.swing.Box;
import javax.swing.JApplet;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.border.BevelBorder;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.MatteBorder;
import javax.swing.border.SoftBevelBorder;
import javax.swing.border.TitledBorder;

/**
 * This is a very simple Java application to demonstrate the basics of
 * Swing. It can be run as an application.
 * 
 * @author cusack
 *
 */
public class SwingExample {
	/**
	 * This method allows us to run this as an application.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		new SwingExample();
	}

	public SwingExample() {
		JFrame window = new JFrame("Nothing");
		Container myContentPane = window.getContentPane();
		makeGUI(myContentPane);
		window.pack();
		window.setVisible(true);
	}

	/**
	 * This method constructs the GUI and places everything on the container
	 * that is passed in.
	 * 
	 * @param container
	 *            the Container to put everything on.
	 */
	public void makeGUI(Container container) {
		// ------------------------------------------------------------------
		// A Plain-Jane label to which we add a simple border.
		//
		JLabel theLabel = new JLabel("This label Doesn't serve much purpose");

		theLabel.setBorder(new EmptyBorder(10, 10, 20, 20));

		// ------------------------------------------------------------------
		// A label that listens for MouseEvents. It is an extension of
		// JLabel that implements MouseListener. It is defined in the file
		// MouseLable.java.
		MouseLabel mouseLabel = new MouseLabel();

		// We also add a nice border to the label
		mouseLabel.setBorder(new MatteBorder(5, 5, 5, 5, Color.green));

		// ------------------------------------------------------------------
		// A special label class to implement MouseMotionListener
		// This is an inner class.
		class MLabel extends JLabel implements MouseMotionListener {
			MLabel() {
			}

			public void mouseDragged(MouseEvent e) {
				setText("Help: I'm Being Dragged");
			}

			public void mouseMoved(MouseEvent e) {
				setText("Coordinates: " + e.getX() + "," + e.getY());
			}
		}
		;
		// ------------------------------------------------------------------

		// ------------------------------------------------------------------
		// We get an instance of the MLabel, and add some text.
		MLabel anotherLabel = new MLabel();
		anotherLabel.setText("Waiting for the mouse to enter....");

		// ------------------------------------------------------------------
		// Although the class for this object implements MouseMotionListener,
		// nowhere in the class is a listener registered.
		// Therefore we have to add a listener, the same object.
		// In other words, the label is essentially saying
		// "If somebody does something with the mouse I am willing to deal
		// with it. Of course somebody needs to tell me about it first.
		// By the way, if somebody does something with the mouse while
		// the mouse is hovering over me, please tell me."
		anotherLabel.addMouseMotionListener(anotherLabel);

		// We also add a more complicated border to the label.
		anotherLabel.setBorder(new CompoundBorder(new TitledBorder(new EmptyBorder(2, 2, 2, 2), "Look: Coordinates.",
				TitledBorder.CENTER, TitledBorder.BOTTOM), new LineBorder(new Color(200, 200, 0), 4)));

		// ------------------------------------------------------------------
		// A new label that does the same as the previous label except that
		// the event handling is done differently.
		// This is just to show you another way of doing things.
		// Since I am accessing fourthLabel from within an
		// (anonymous) inner class (below), it has to be final.
		// Alternatively, it could be a class variable (field).

		final JLabel fourthLabel = new JLabel();

		fourthLabel.setText("blah            ");
		fourthLabel.addMouseMotionListener(new MouseMotionListener() {
			public void mouseDragged(MouseEvent e) {
				fourthLabel.setText("Help: I'm Being Dragged");
			}

			public void mouseMoved(MouseEvent e) {
				fourthLabel.setText("Coordinates: " + e.getX() + "," + e.getY());
			}

		});
		fourthLabel.setBorder(new SoftBevelBorder(BevelBorder.RAISED, Color.red, Color.blue));

		// ------------------------------------------------------------------
		// A box to put labels on A box is a container that things can be
		// placed on. Each new item is placed on the box after the previous
		// item. Depending on whether it is a vertical or horizontal box,
		// the items will do from top to bottom, or left to right.
		Box labelBox = Box.createVerticalBox();

		// ------------------------------------------------------------------
		// Add the labels to a box, in the order we wish them to appear on
		// the box.
		labelBox.add(theLabel);
		labelBox.add(mouseLabel);
		labelBox.add(anotherLabel);
		labelBox.add(fourthLabel);

		// ------------------------------------------------------------------
		// A button to demonstrate actionListener
		JButton someButton = new JButton("Press me");

		// mouseLabel wants to listen for events from someButton.
		someButton.addActionListener(mouseLabel);

		// ------------------------------------------------------------------
		// An exit button. Bad for an applet, since you can't exit.
		// This is why it is labeled "don't press me".
		JButton anotherButton = new JButton("don't press me");
		anotherButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});

		// ------------------------------------------------------------------
		// A box to put the buttons on.
		Box buttonBox = Box.createVerticalBox();
		buttonBox.add(someButton);
		buttonBox.add(anotherButton);

		// ------------------------------------------------------------------
		// The ContentPane needs to know how to arrange the object you place
		// on it. A layout manager takes care of this. This command sets
		// the layout manager to a border layout.
		// BorderLayout is usually the "top level" layout manager that I use.
		container.setLayout(new BorderLayout());

		// ------------------------------------------------------------------
		// Now I place the various components on the ContentPane. For a
		// BorderLayout, you must specify a region, which is one of NORTH,
		// SOUTH, EAST, WEST, or CENTER, and may only place one object per
		// region.
		// Whatever you place in CENTER will expand and take up as much space
		// as possible.
		container.add(buttonBox, BorderLayout.EAST);
		container.add(labelBox, BorderLayout.CENTER);
	}
}
